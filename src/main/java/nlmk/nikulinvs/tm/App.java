package nlmk.nikulinvs.tm;


import nlmk.nikulinvs.tm.controller.ProjectController;
import nlmk.nikulinvs.tm.controller.SystemController;
import nlmk.nikulinvs.tm.controller.TaskController;
import nlmk.nikulinvs.tm.repository.ProjectRepository;
import nlmk.nikulinvs.tm.repository.TaskRepository;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.ProjectTaskService;
import nlmk.nikulinvs.tm.service.TaskService;

import java.util.Scanner;

import static nlmk.nikulinvs.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */

public class App {

    private ProjectRepository projectRepository = new ProjectRepository();

    private ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private TaskRepository taskRepository = new TaskRepository();

    private TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository,taskRepository);

    private TaskController taskController = new TaskController(taskService,projectTaskService);

    private final SystemController systemController = new SystemController();

     {
        projectRepository.create("PROJECT DEMO 1");
        projectRepository.create("PROJECT DEMO 2");
        taskRepository.create("TASK DEMO 1");
        taskRepository.create("TASK DEMO 2");
    }

    public static void main(final String[] args) {

        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcom();
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }


    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case CMD_VERSION: return systemController.displayVersion();
            case CMD_ABOUT: return systemController.displayAbout();
            case CMD_HELP: return systemController.displayHelp();
            case CMD_EXIT: return systemController.displayExit();

            case CMD_PROJECT_CREATE: return projectController.createProject();
            case CMD_PROJECT_CLEAR: return projectController.clearProject();
            case CMD_PROJECT_LIST: return projectController.listProject();
            case CMD_PROJECT_VIEW: return projectController.viewProjectByIndex();
            case CMD_PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case CMD_PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case CMD_PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case CMD_PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case CMD_TASK_CREATE: return taskController.createTask();
            case CMD_TASK_CLEAR: return taskController.clearTaskt();
            case CMD_TASK_LIST: return taskController.listTask();
            case CMD_TASK_VIEW: return taskController.viewTaskByIndex();
            case CMD_TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case CMD_TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case CMD_TASK_REMOVE_BY_INDEX: return taskController.removeTasktByIndex();
            case CMD_TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case CMD_TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskFromProjectByIds();
            case CMD_TASK_LIST_BY_PROJECT_ID: return taskController.ListTaskByProjectId();

            default: return systemController.displayError();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
