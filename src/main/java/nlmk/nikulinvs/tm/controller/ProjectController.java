package nlmk.nikulinvs.tm.controller;

import nlmk.nikulinvs.tm.repository.ProjectRepository;
import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.service.*;

public class ProjectController extends AbstractController{

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description");
        final String description = scanner.nextLine();
        projectService.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("Please, enter index number");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description");
        final String description = scanner.nextLine();
        projectService.update(project.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY NAME:" + name + " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("Please, enter ID number");
        final Long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY ID:" + id + " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("Please, enter INDEX number");
        final int id = scanner.nextInt() -1;
        final Project project = projectService.removeByIndex(id);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY INDEX:" + id + " IS NOT EXIST]" );
        else System.out.println("[OK]");
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEARE PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public  void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID:-> " + project.getId());
        System.out.println("NAME:-> " + project.getName());
        System.out.println("DESCRIPTION:-> " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex () {
        System.out.println("ENTER PROJECT INDEX");
        final int index  = scanner.nextInt() -1;
        final Project project = projectService.findByIndex(index);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY INDEX:" + (index+1) + " IS NOT EXIST!]");
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("ENTER PROJECT INDEX");
        final int id  = scanner.nextInt() -1;
        final Project project = projectService.findByIndex(id);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY INDEX:" + (id+1) + " IS NOT EXIST!]");
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project : projectService.findAll()) {
            System.out.println(index + ") " + project.getId() + " : " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}
