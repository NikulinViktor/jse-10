package nlmk.nikulinvs.tm.controller;

public class SystemController {

    public int displayExit() {
        System.out.println("Terminate programm...");
        return 0;
    }

    public int displayError() {

        System.out.println("Error! Unknown programm arguments ...");
        return -1;
    }

    public void displayWelcom() {

        System.out.println("**Welcom to Task Manager**");
    }

    public int displayHelp() {
        System.out.println("|-------HELP LIST--------------------------|");
        System.out.println("version - Display programm version");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of terminal commands");
        System.out.println("exit - Terminate Task-manager!");
        System.out.println("--------PROJECT------------------------------");
        System.out.println("project-create - Create new project by name");
        System.out.println("project-clear - Remove all projects");
        System.out.println("project-list - Display list of projects");
        System.out.println("pproject-remove-by-name - Remov exist projects by name");
        System.out.println("pproject-remove-by-id - Remov exist projects by Id");
        System.out.println("pproject-remove-by-index - Remov exist projects by index");
        System.out.println("pproject-update-by-index - Update exist projects by index");
        System.out.println("--------TASK----------------------------------");
        System.out.println("task-create - Create new task by name");
        System.out.println("task-clear - Remove all tasks");
        System.out.println("task-list - Display list of tasks");
        System.out.println("task-remove-by-name - Remov exist task by name");
        System.out.println("task-remove-by-id - Remov exist task by Id");
        System.out.println("task-remove-by-index - Remov exist task by index");
        System.out.println("task-update-by-index - Update exist taskhelp by index");
        System.out.println("task-list-by-project-id - Display Task list by Project ID");
        System.out.println("task-add-to-project-by-ids - Add task to Project by Ids");
        System.out.println("task-remove-from-project-by-ids - Remove from Project by Ids");

        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.1");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Nikulin Viktor");
        System.out.println("nikulin_vs@nlmk.com");
        return 0;
    }
}
