package nlmk.nikulinvs.tm.controller;

import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.ProjectTaskService;
import nlmk.nikulinvs.tm.service.TaskService;

import java.util.List;

public class TaskController extends AbstractController {


    private final TaskService taskService;


    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Please, enter task name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskService.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Please, enter index number");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Please, enter task name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskService.update(task.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null)
            System.out.println("[FAILE -> TASK BY NAME:" + name + " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("Please, enter ID number");
        final Long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null)
            System.out.println("[FAILE -> TASK BY ID:" + (id+1)+ " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTasktByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("Please, enter INDEX number");
        final int id = scanner.nextInt() -1;
        final Task task = taskService.removeByIndex(id);
        if (task == null)
            System.out.println("[FAILE -> TASK BY INDEX:" + (id+1) + " IS NOT EXIST]" );
        else System.out.println("[OK]");
        return 0;
    }

    public int clearTaskt() {
        System.out.println("[CREATE TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listTask() {
        System.out.println("[CREATE TASK]");
        int index = 1;
        viewTask(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID:-> " + task.getId());
        System.out.println("NAME:-> " + task.getName());
        System.out.println("DESCRIPTION:-> " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex () {
        System.out.println("ENTER TASK INDEX");
        final int index  = scanner.nextInt()-1;
        final Task task = taskService.findByIndex(index);
        if (task == null)
            System.out.println("[FAILE -> TASK BY INDEX:" + (index+1) + " IS NOT EXIST!]");
        viewTask(task);
        return 0;
    }

    public void viewTask(final List<Task> tasks){
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ") " + task.getId() + " : " + task.getName());
            index ++;
        }
    }

    public int ListTaskByProjectId() {
        System.out.println("[LIST TASk BY PROJECT]");
        System.out.println("Please, enter project ID number");
        final Long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectID(projectId);
        if (tasks == null)
            System.out.println("Task of ProjectId not faound. Try egaine or put exit to terminate");
        viewTask(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASk TO PROJECT BY IDS]");
        System.out.println("Please, enter project ID number");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter task ID number");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId,taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectByIds() {
        System.out.println("[REMOVE TASk FROM PROJECT BY IDS]");
        System.out.println("Please, enter project ID number");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter task ID number");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskByProject(projectId,taskId);
        System.out.println("[OK]");
        return 0;
    }

}
