package nlmk.nikulinvs.tm.service;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;
import nlmk.nikulinvs.tm.repository.ProjectRepository;
import nlmk.nikulinvs.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAddByProjectID(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAddByProjectID(projectId);
    }

    public Task removeTaskByProject (final Long projectId, final Long taskid) {
         final Task task = taskRepository.findByProjectIdAnId(projectId,taskid);
         if (task == null) return null;
         task.setProjectId(null);
         return task;
    }

    public Task addTaskToProject (final Long projectId, final Long taskid) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskid);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }
}
